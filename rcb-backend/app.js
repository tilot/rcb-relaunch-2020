const express = require('express');
const fs = require('fs')
const { graphqlHTTP } = require('express-graphql');

require('dotenv').config();

const mongoose = require("mongoose")

const root = require('./src/resolver')
const schema = require('./src/schema');
const dev = process.env.NODE_ENV != 'production'
const isAuth = require("./src/middleware/is-auth")

var app = express();
app.use(isAuth)
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader('Access-Control-Allow-Methods', 'POST,GET,OTIONS')
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization')
    if(req.method==='OPTIONS') {
        return res.sendStatus(200)
    }
    next()
})
app.use(express.static('public'));

app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: dev,
  pretty: dev
}));


mongoose.set('useCreateIndex', true)
mongoose.set('debug', true)
const mongodb_server = process.env.MONGODB_SERVER || 'localhost'
const mongodb_port = process.env.MONGODB_PORT || 27017
const mongodb_name = process.env.MONGODB_NAME || 'rcb-backend'
const mongodb_uri = `mongodb://${mongodb_server}:${mongodb_port}/${mongodb_name}`
const mongodb_options = {
        sslValidate: true,
        ssl: true,
        sslKey: fs.readFileSync('../ssl/itilos-forge/mongodb.pem'),
        sslCA: fs.readFileSync('../ssl/itilos-forge/itilos-forge.ca.pem'),
		useNewUrlParser: true,
        useUnifiedTopology: true,
        authSource: "admin",
        user: "rcb",
        pass: "Moderator",
        poolSize: 10
	}
	
console.log('connecting to mongodb '+mongodb_uri)

mongoose.connect(mongodb_uri, mongodb_options).then(() => {
    const port = process.env.PORT || 4000
    const bind_address = process.env.BIND_ADDRESS || '127.0.0.1'
    console.log('listen: '+bind_address+':'+port)
    app.listen(port, bind_address)
}).catch(err => {
    console.log(err)
});



