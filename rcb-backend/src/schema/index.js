// Schemas

var { buildSchema } = require('graphql');


module.exports = buildSchema(`
	scalar Date

	type User {
		id: ID!
		email: String!
		firstName: String
		lastName: String
		token: String
		tokenExpiration: Int
	}
	input UserInput {
		id: ID!
		email: String!
		firstName: String
		lastName: String
		token: String
		tokenExpiration: Int
	}
	type Abonnent {
		id: ID!
		email: String!
		firstName: String
		lastName: String
		expires: Date!
	}

	type Query {
		login(email: String!, passwd: String!): User

		findUser(start: Int!, limit: Int!): [User!]!

		findAbonnenten(start: Int!, limit: Int!): [Abonnent!]!
	}

	type Mutation {
		createAbonnent (
			email: String!,
			firstName: String,
			lastName: String,
			expire: Date
		): Abonnent,
		saveUser(user: UserInput!): Boolean!,
		deleteUser(userId: ID!): Boolean!,

		createUser (
			email: String!,
			firstName: String,
			lastName: String,
			pwd: String!
		): User
	}`);


