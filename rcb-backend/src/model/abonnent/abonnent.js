const mongoose = require('mongoose')

const Schema = mongoose.Schema

const AbonnentSchema = new Schema({
    mandant:  {
        required: true,
        type: Schema.Types.ObjectId,
        ref: 'Mandant'
    },
    email: {
        type: String,
        required: true
    },
    expireDate: {
        type: Date,
        required: true
    },
    firstName: {
        type: String,
        required: false
    },
    lastName: {
        type: String,
        required: false
    },
    gender: {
        type: Number,
        required: true
    }
}, { timestamps: true});

AbonnentSchema.index({ email: 1 });

module.exports = mongoose.model('Abonnent', AbonnentSchema)
