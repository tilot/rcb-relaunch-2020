const mongoose = require('mongoose')

const Schema = mongoose.Schema

const MandantSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    }
}, { timestamps: true});

MandantSchema.index({ email: 1 });

module.exports = mongoose.model('Mandant', MandantSchema)
