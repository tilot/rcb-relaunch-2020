const mongoose = require('mongoose')

const Schema = mongoose.Schema

const UserSchema = new Schema({
    mandant:  {
        required: true,
        type: Schema.Types.ObjectId,
        ref: 'Mandant'
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    firstName: {
        type: String,
        required: false
    },
    lastName: {
        type: String,
        required: false
    },
    administrator: {
        type: Boolean,
        required: true
    },
    gender: {
        type: Number,
        required: true
    }
}, { timestamps: true});

UserSchema.index({ email: 1 });

module.exports = mongoose.model('User', UserSchema)
