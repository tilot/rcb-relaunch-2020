const userController = require("./../../../controller/user")
const abonnentController = require("./../../../controller/abonnent")
const mandantController = require("./../../../controller/mandant")

const moment = require("moment")

function toMandantInput(email, name) {
	return { mandantInput: {
		email,
		name
	}}
}

function toUserInput(mandant, email, firstName, lastName, gender, password, administrator) {
	return { userInput: {
		mandant: mandant._id,
		email,
		firstName,
		lastName,
		password,
		gender,
		administrator
	}}
}

function toAbonnentInput(mandant, email, firstName, lastName, gender) {
	return { abonnentInput: {
		mandant: mandant._id,
		email,
		firstName,
		lastName,
		gender,
		expireDate: moment().add(100, 'days').calendar()
	}}
}

async function tryCreateUser(u) {
	try {
		await userController.createUser(u)
	} catch(err) {
		console.log(err);
	}
}
async function tryCreateAbonnent(a) {
	try {
		await abonnentController.createAbonnent(a)
	} catch(err) {
		console.log(err);
	}
}
async function tryCreateMandant(m) {
	try {
		return await mandantController.createMandant(m)
	} catch(err) {
		console.log(err);
	}
}

module.exports = async function() {
	console.log("start initializing")
	const mandant = await tryCreateMandant(toMandantInput("president@rc-bgdf.de", "Rednerclub Bergedorf"))

	await tryCreateUser(toUserInput(mandant, "tilo.thiele@hamburg.de", "Tilo", "Thiele", 1, "Start$1234", false))
	await tryCreateUser(toUserInput(mandant, "tilothiele16@gmail.com", "Administrator", "Administrator", 1, "Start$1234", true))
	
	await tryCreateAbonnent(toAbonnentInput(mandant, "theo.test@gmail.com", "Theo", "Tester", 1))
	await tryCreateAbonnent(toAbonnentInput(mandant, "lady.gaga@gmail.com", "Lady", "Gaga", 2))
	await tryCreateAbonnent(toAbonnentInput(mandant, "markus.krebs@gmail.com", "Markus", "Krebs", 1))
	await tryCreateAbonnent(toAbonnentInput(mandant, "barack.obama@gmail.com", "Barack", "Obama", 1))
	await tryCreateAbonnent(toAbonnentInput(mandant, "sean.connory@gmail.com", "Sean", "Connory", 1))
	
	console.log("finished initializing")
}