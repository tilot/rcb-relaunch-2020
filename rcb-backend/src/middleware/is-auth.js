const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
    const authHeader = req.get('Authorization')

    if(!authHeader) {
        req.isAuth = false
        return next()
    }
    const token = authHeader //.split(' ')[1]
    if(!token || token==='') {
        req.isAuth = false
        return next()
    }
    let decrypted
    try {
        decrypted = jwt.verify(token, process.env.JWT_SECRET)
    } catch(err) {
        req.isAuth = false
        return next()
    }
    if(!decrypted) {
        req.isAuth = false
        return next()
    }
    req.isAuth = true
    req.userId = decrypted.userId
    next()
}