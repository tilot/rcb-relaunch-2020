const Abonnent = require("./../../model/abonnent/abonnent")
const EmailValidator = require('email-validator')

async function createAbonnent({abonnentInput}) {
        if(abonnentInput.email && !EmailValidator.validate(abonnentInput.email)) {
            throw new Error("invalid email address")
        }
        const msg = abonnentInput.password && validatePwd(abonnentInput.password)
        if(abonnentInput.password && !msg.valid) {
            msg.messages.forEach(m => console.log(m))
            throw new Error("password not accepted")
        }

        return Abonnent.findOne({email: abonnentInput.email})
            .then(abonnent => {
                if(abonnent) {
                    throw new Error('Abennent already defined')
                }
                const rv = new Abonnent({
                    ...abonnentInput
                })
                return rv.save()
            }).then(result => {
                return { ...result._doc, _id: result.id}
            // }).catch(err => {
            //     console.log(err)
            //     throw err;
            })
}

async function findAbonnenten({start, limit}, req) {
    start = start || 0
    limit = Math.min(limit || 100, 500)
    return await Abonnent.find({}).skip(start).limit(limit)
}

module.exports = {
    createAbonnent,
    findAbonnenten
}