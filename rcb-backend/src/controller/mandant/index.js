const Mandant = require("./../../model/mandant/mandant")
const EmailValidator = require('email-validator')

async function createMandant({mandantInput}) {
        if(mandantInput.email && !EmailValidator.validate(mandantInput.email)) {
            throw new Error("invalid email address")
        }

		return Mandant.findOne({email: mandantInput.email})
            .then(mandant => {
                if(mandant) return mandant;
                const rv = new Mandant({
                    ...mandantInput
                })
                return rv.save()
            }).then(result => {
                return { ...result._doc, _id: result.id}
            // }).catch(err => {
            //     console.log(err)
            //     throw err;
            })
}

module.exports = {
	createMandant
}