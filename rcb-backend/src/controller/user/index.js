const bcryptjs = require("bcrypt")
const jwt = require("jsonwebtoken")
const User = require("./../../model/user/user")
const EmailValidator = require('email-validator')
const lodash = require("lodash")

const validatePwd = (pwd) => {
    const msgs = []
    if(!pwd) msgs.push({ key: 'empty-password-not-allowed'})
    if(pwd.length<5 || pwd.length>12) msgs.push({ key: 'password-has-invalid-length', args: ['5', '12']})
    if(!/[0-9]/.test(pwd)) msgs.push({ key: 'password-needs-num'})
    if(!/[a-z]/.test(pwd)) msgs.push({ key: 'password-needs-lower-case'})
    if(!/[A-Z]/.test(pwd)) msgs.push({ key: 'password-needs-upper-case'})
    if(!/[!"§$%&/()=\-#+*<>_]/.test(pwd)) msgs.push({ key: 'password-needs-special-char', args: ['!"§$%&/()=-#+*<>_']})
    return {
        valid: msgs.length==0,
        messages: msgs
    }
}


function transformUser(mandant, id, email, firstName, lastName, token, administrator) {
    return {
            mandant,
            id,
            email,
            firstName,
            lastName,
            token,
            administrator,
            tokenExpiration: 1
        }	
}

function transformUserEntity(u) {
    return {
            mandant: u.mandant._id.toString(),
            id: u._id.toString(),
            email: u.email,
            firstName: u.firstName,
            lastName: u.lastName,
            administrator: u.administrator
        }	
}

async function login({email, passwd}) {
        const user = await User.findOne({email});
        if(!user) {
            throw new Error('User does not exist')
        }
        const isEq = await bcryptjs.compare(passwd, user.password)
        if(!isEq) {
            throw new Error('Pwd not correct')
        }
        //console.log(process.env)
        const data = { ...user}
        const secret = process.env.JWT_SECRET
        const token = await jwt.sign(data, secret, {
            expiresIn: '1h'
        })
        //console.log(user)
        const rv = transformUser(user.mandant._id.toString(), user.id, user.email, user.firstName, user.lastName, token, user.administrator)
        return rv
}

async function createUser({userInput}) {
        if(userInput.email && !EmailValidator.validate(userInput.email)) {
            throw new Error("invalid email address")
        }
        const msg = userInput.password && validatePwd(userInput.password)
        if(userInput.password && !msg.valid) {
            msg.messages.forEach(m => console.log(m))
            throw new Error("password not accepted")
        }

        console.log(userInput)

        return User.findOne({email: userInput.email})
            .then(user => {
                if(user) {
                    throw new Error('User already defined')
                }
                console.log("pwd", userInput.password)
                return bcryptjs.hash(userInput.password, 12)
            }).then(hashedPassword => {
                const user = new User({
                    ...userInput,
                    password: hashedPassword
                })
                return user.save()
            }).then(result => {
                return { ...result._doc, password: null, _id: result.id}
            // }).catch(err => {
            //     console.log(err)
            //     throw err;
            })
}

async function findUser({start, limit}, req) {
    start = start || 0
    limit = Math.min(limit || 100, 500)
    const rv = await User.find({}).skip(start).limit(limit)
    return lodash.map(rv, u => transformUserEntity(u))
}

module.exports = {
    login,
    createUser,
    findUser
}