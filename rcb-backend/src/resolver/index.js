// Resolvers
const user = require("../controller/user")
const abonnenten = require("../controller/abonnent")

module.exports = {
  ...user,
  ...abonnenten
};


