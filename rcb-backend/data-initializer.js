
require('dotenv').config();

const mongoose = require("mongoose")

const dataInitializer = require("./src/module/initialize/dbdata")

mongoose.set('useCreateIndex', true)
mongoose.set('debug', true)
const mongodb_server = process.env.MONGODB_SERVER || 'localhost'
const mongodb_port = process.env.MONGODB_PORT || 27017
const mongodb_name = process.env.MONGODB_NAME || 'rcb-backend'
const mongodb_uri = `mongodb://${mongodb_server}:${mongodb_port}/${mongodb_name}`
const mongodb_options = {
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
	
console.log('connecting to mongodb '+mongodb_uri)

mongoose.connect(mongodb_uri, mongodb_options).then(() => {
    console.log('connected to db')
    return dataInitializer()
}).then(() => {
    console.log('disconnecting...')
    return mongoose.disconnect();
}).then(() => {
    console.log('disconnected')
}).catch(err => {
    console.log(err)
    return mongoose.disconnect();
});



