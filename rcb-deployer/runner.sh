#!/bin/bash

set -x
set -e

pushd $(dirname $0)
BINDIR=$(pwd)


echo $BINDIR

. $BINDIR/.env.deployer

whoami
mkdir -p ~/.ssh
ls -la ~
if [ ! -n "$(grep "^bitbucket.org " ~/.ssh/known_hosts)" ]; then ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts 2>/dev/null; fi
ls -l ~/.ssh

while [ -t ]
do

	if [ ! -f $TRIGGER_FILE ]; then
		echo "$TRIGGER_FILE nicht vorhanden..."
		sleep 10
		continue
	fi

	rm $TRIGGER_FILE

	if [ -d $LOCAL_REPOSITORY_DIR/rcb-relaunch-2020/.git ]; then
		pushd $LOCAL_REPOSITORY_DIR/rcb-relaunch-2020
		git pull
#		sshpass -p $PASSWD git pull
	else
		mkdir -p $LOCAL_REPOSITORY_DIR
		pushd $LOCAL_REPOSITORY_DIR
		git clone $REPOSITORY_URL
		cd rcb-relaunch-2020
	fi

	cd rcb-homepage
	cp $BINDIR/.env.production .env
	npm install
	npm run deploy

done