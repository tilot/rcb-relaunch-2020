# Rednerclub Bergedorf Homepage

node-gyp läuft nicht unter node v16.x.x - Daher zurückgedreht auf die 12er Version

## Entwicklungsumgebung

### VS Code 

Plugins

* bracket pair colorizer
* VS Code ES7 React/Redux/React-Native/JS snippets
* Highlight Matching Tag
* vscode-styled-components
* Prettier - Code formatter

### Resources

* iconfinder
* strapi
* heroku
* atlas mongodb

