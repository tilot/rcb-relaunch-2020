


## Lokal

```
#Docker-image bauen
docker build . -t tilothiele/rcb-publish-api

# starten
docker run -v /home/tilot/scratch/touchfiledir:/usr/src/touchfiledir -p 3000:3000 tilothiele/rcb-publish-api

```

## Remote / produktiv

Wird auf der lokalen Entwicklungsmaschine ausgeführt

```
# deployment
DOCKER_HOST="ssh://tilo@h2931691.stratoserver.net" docker build . -t tilothiele/rcb-publish-api

# starten
ssh tilo@h2931691.stratoserver.net docker run -d -v /home/tilo/runner-working-dir:/usr/src/touchfiledir -p 3000:3000 --restart unless-stopped tilothiele/rcb-publish-api

```

Alternativ siehe

https://stackoverflow.com/questions/23935141/how-to-copy-docker-images-from-one-host-to-another-without-using-a-repository
